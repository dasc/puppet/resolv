class resolv (
    Array[String] $nameserver,
    Array[String] $search,
    Array[String] $options,
    Optional[String] $domain = undef,
) {
    file { 'resolv.conf':
        path => "/etc/resolv.conf",
        ensure => file,
        owner => 'root',
        group => 'root',
        mode => '0644',
        content => epp('resolv/etc/resolv.conf.epp', {
            'resolv_search' => $search,
            'resolv_domain' => $domain,
            'resolv_nameserver' => $nameserver,
            'resolv_options' => $options,
        }),
    }

}
