# resolv

#### Table of Contents

1. [Description](#description)
1. [What resolv affects](#what-resolv-affects)
1. [Usage - Configuration options and additional functionality](#usage)

## Description

Manage the DNS resolver file resolv.conf.

### What resolv affects

This module updates the resolv.conf file, and nothing else.

## Usage

To create a resolv.conf file with two nameservers, a default domain, and multple search domains, use the folowing code:

```
   class {'::resolv':
        nameserver => ['10.13.5.17', '10.13.5.18',],
        search => ['ldas.ligo-la.caltech.edu',],
        domain => 'ldas.ligo-la.caltech.edu',
   }
```

You can also specify additional options to put in the resolv.conf file, such as timeouts:
```
   class {'::resolv':
        nameserver => ['10.13.5.17', '10.13.5.18',],
        search => ['ldas.ligo-la.caltech.edu',],
        domain => 'ldas.ligo-la.caltech.edu',
        options => ['timeout:1', 'attempts:5', 'rotate',],
   }
```
